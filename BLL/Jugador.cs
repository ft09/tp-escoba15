﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        MP_Jugador mpjug = new MP_Jugador();
        List<BE.Jugador> listJugPartida = new List<BE.Jugador>();

        public void RegistrarUsuario(BE.Jugador jugador)
        {
            mpjug.Insertar(jugador);
        }

        public bool Listar(string usuario, string contraseña)
        {
            bool existe = false;
            
            foreach (var item in mpjug.ListarJugadores())
            {
                if (item.Usuario == usuario & item.Contraseña == contraseña)
                {
                    listJugPartida.Add(item);
                    existe = true;
                }
            }
            return existe ;
        }
        public List<BE.Jugador> ListarJugadores()
        {
            return listJugPartida;
        }

    }
}
