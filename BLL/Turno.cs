﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
   public class Turno
    {
        BE.Turno t = new BE.Turno();
        DAL.Juego j = new DAL.Juego();
        public List<BE.Carta> Repartir(List<BE.Carta> mazo)
        {

            //sacar carta del mazo
            t.ListJugador1 = new List<BE.Carta>();
            t.ListJugador2 = new List<BE.Carta>();

            //cada jugador tiene una lista de cartas


            int cont = 0;

            for (int i = 1; i <= 3; i++)
            {
                BE.Carta cartaDeMazo;

                cartaDeMazo = mazo.First();
                cartaDeMazo.Ruta = j.RutaImagen(cartaDeMazo.Palo, cartaDeMazo.Numero);
                t.ListJugador1.Add(cartaDeMazo);
                mazo.RemoveAt(0);

                cartaDeMazo = mazo.First();
                cartaDeMazo.Ruta = j.RutaImagen(cartaDeMazo.Palo, cartaDeMazo.Numero);
                t.ListJugador2.Add(cartaDeMazo);
                mazo.RemoveAt(0);

                cont++;
            }
            return t.ListJugador1;
            //agregar carta a una lista de cada jugador con un ciclo for que sea 3 veces
        }
        public List<BE.Carta> Mazojugador1()
        {
            return t.ListJugador1;
        }
        public List<BE.Carta> Mazojugador2()
        {
            return t.ListJugador2;
        }

      public List<BE.Carta> CartasMesa(List<BE.Carta> mazo)
        {
            BE.Carta cartaDeMazo;
            for (int i = 0; i <= 3; i++)
            {
                
                cartaDeMazo = mazo.First();
                cartaDeMazo.Ruta = j.RutaImagen(cartaDeMazo.Palo, cartaDeMazo.Numero);
                t.listCarMesa.Add(cartaDeMazo);
                mazo.RemoveAt(0);
            }

            return t.listCarMesa;
        }
    }

}
