﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Tablero
    {
        DAL.Juego J = new DAL.Juego();
        public delegate void delEnviarCasillero(Casillero casillero);

        public event delEnviarCasillero EnviarCasillero;

        public void Repartir(List<BE.Carta> cartas, int columna, int x, int y)
        {
            var index = 0;
            foreach (var carta in cartas)
            {
                Casillero casillero = new Casillero();
                casillero.X = index;
                casillero.Y = columna * 2 ;
                casillero.Palo = carta.Palo;
                casillero.Numero = carta.Numero;
                casillero.Posx = x;
                casillero.Posy = y;
                casillero.Imagen = carta.Ruta;

                this.EnviarCasillero(casillero);

                index++;
            }
        }
        string BuscarImg(int numero, string palo)
        {
            
            return J.RutaImagen(palo, numero);
        }
    }
}
