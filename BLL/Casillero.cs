﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Casillero
    {
        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        private string imagene;

        public string Imagen
        {
            get { return imagene; }
            set { imagene = value; }
        }

        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string palo;

        public string Palo
        {
            get { return palo; }
            set { palo = value; }
        }

        private int posx;

        public int Posx
        {
            get { return posx; }
            set { posx = value; }
        }

        private int posy;

        public int Posy
        {
            get { return posy; }
            set { posy = value; }
        }
    }
}
