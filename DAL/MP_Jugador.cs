﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using BE;
using System.Security.Cryptography;

namespace DAL
{
    public class MP_Jugador
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Jugador jugador)
        {
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();

            parameters.Add(acceso.CrearParametro("@Nom", jugador.Nombre));
            parameters.Add(acceso.CrearParametro("@Ape", jugador.Apellido));
            parameters.Add(acceso.CrearParametro("@FechaNac", jugador.FechaNacimiento));
            parameters.Add(acceso.CrearParametro("@Usuario", jugador.Usuario));
            parameters.Add(acceso.CrearParametro("@Contr", jugador.Contraseña));

            acceso.Escribir("RegistrarUsuario", parameters);

            acceso.Cerrar();
        }

        public List<Jugador> ListarJugadores()
        {
            List<Jugador> lista = new List<Jugador>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ListarJugadores");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                Jugador j = new Jugador();
                j.Usuario = registro[0].ToString();
                j.Contraseña = registro[1].ToString();
                j.Nombre = registro[2].ToString();
                j.Apellido = registro[3].ToString();

                j.FechaNacimiento = DateTime.Parse(registro[4].ToString());
                lista.Add(j);
            }
            return lista;
        }
    }
}
