﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace DAL
{
   public class Juego
    {
        public string RutaImagen(string palo, int numero)
        {
            Acceso acceso = new Acceso();
            acceso.Abrir();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            BE.Carta c = new BE.Carta();
            parameters.Add(acceso.CrearParametro("@Palo", palo));
            parameters.Add(acceso.CrearParametro("@Num", numero));
            //List<Carta> lista = new List<Carta>();
            
            
            DataTable tabla = acceso.Leer("ListarRuta", parameters);
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {
                c.Ruta = registro[0].ToString();
            }

            return c.Ruta;
        }
    }
}
