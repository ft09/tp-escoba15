﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
   public class Jugador:Persona
    {
        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string contraseña;

        public string Contraseña
        {
            get { return contraseña; }
            set { contraseña = value; }
        }

        private DateTime inicioSesion;

        public DateTime InicioSesion
        {
            get { return inicioSesion; }
            set { inicioSesion = value; }
        }

        private DateTime finSesion;

        public DateTime FinSesion
        {
            get { return finSesion; }
            set { finSesion = value; }
        }

        private DateTime comienzoPartida;

        public DateTime ComienzoPartida
        {
            get { return comienzoPartida; }
            set { comienzoPartida = value; }
        }

        private DateTime finPartida;

        public DateTime FinPartida
        {
            get { return finPartida; }
            set { finPartida = value; }
        }
        private DateTime tiempoTotal;

        public DateTime TiempoTotal
        {
            get { return tiempoTotal; }
            set { tiempoTotal = value; }
        }

        private int ganado;

        public int Ganado
        {
            get { return ganado; }
            set { ganado = value; }
        }

        private int perdido;

        public int Perdido
        {
            get { return perdido; }
            set { perdido = value; }
        }

        private int empatado;

        public int Empatado
        {
            get { return empatado; }
            set { empatado = value; }
        }
    }
}
