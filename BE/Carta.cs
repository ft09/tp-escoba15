﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Carta
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        private string palo;

        public string Palo
        {
            get { return palo; }
            set { palo = value; }
        } 
        
        private string ruta;

        public string Ruta
        {
            get { return ruta; }
            set { ruta = value; }
        }

        //private List<string> cartas;

        //public List<string> Cartas
        //{
        //    get { return cartas; }
        //    set { cartas = value; }
        //}

        //private List<string> listaMazo;

        //public List<string> ListaMazo
        //{
        //    get { return listaMazo; }
        //    set { listaMazo = value; }
        //}

        //List<Carta> listaCartas = new List<Carta>();
    }
}
