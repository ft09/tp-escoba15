﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace Escoba_de_15
{
    public partial class CUCarta : UserControl
    {
        private Casillero CASILLERO;

        public Casillero Casillero
        {
            get { return CASILLERO; }
            set { CASILLERO = value; }
        }
        public delegate void delClick(CUCarta ficha);

        public event delClick FichaClick;


        public CUCarta()
        {
            InitializeComponent();
        }

        private void Carta_Load(object sender, EventArgs e)
        {

        }

        public void EstablecerImagen(string ruta)
        {      
           pictureBox1.Image = Image.FromFile(ruta);

        }


        private void pictureBox1_Click_2(object sender, EventArgs e)
        {
            this.FichaClick(this);
        }

        private void CUCarta_Click(object sender, EventArgs e)
        {

        }
    }
}
