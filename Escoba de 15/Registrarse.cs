﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escoba_de_15
{
    public partial class Registrarse : Form
    {
        BLL.Jugador jugador = new BLL.Jugador();
        BE.Jugador j = new BE.Jugador();
        public Registrarse()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            j.Nombre = txtNombre.Text;
            j.Apellido = txtApellido.Text;
            j.FechaNacimiento = dateTimePicker1.Value;
            j.Usuario = txtUsuario.Text;
            j.Contraseña = txtContra.Text;
            jugador.RegistrarUsuario(j);
        }
    }
}
