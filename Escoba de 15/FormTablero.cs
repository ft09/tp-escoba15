﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escoba_de_15
{
    public partial class FormTablero : Form
    {
        BLL.Mazo m = new BLL.Mazo();
        BE.Mazo mazo;
        BLL.Turno t = new BLL.Turno();
        List<BE.Carta> ListMazo = new List<BE.Carta>();
        List<BE.Carta> ListMazo2 = new List<BE.Carta>();
        List<BE.Carta> ListaSeleccionada = new List<BE.Carta>();
        BLL.Tablero tablero = new BLL.Tablero();

        public List<BE.Jugador> jugadores = new List<BE.Jugador>();

        public FormTablero()
        {
            InitializeComponent();

        }
        void Prueba()
        {
            tablero.EnviarCasillero += Tablero_EnviarCasillero;

            t.Repartir(ListMazo);
            tablero.Repartir(t.Mazojugador1(), 0, 700, 0);
            tablero.Repartir(t.Mazojugador2(), 1, 700, 300);
            tablero.Repartir(t.CartasMesa(ListMazo), 1, 650, 25);
        }
        private void Tablero_Load(object sender, EventArgs e)
        {
            m.CargarPalos();
            m.CrearMazo();
            ListMazo = m.DevuelveMazo();
            m.Mezclar(ListMazo);
            
            tablero.EnviarCasillero += Tablero_EnviarCasillero;

            t.Repartir(ListMazo);
            tablero.Repartir(t.Mazojugador1(), 0, 700, 0);
            tablero.Repartir(t.Mazojugador2(), 1, 700, 300);
            tablero.Repartir(t.CartasMesa(ListMazo), 1, 650, 25);
        }
        private void Tablero_EnviarCasillero(Casillero casillero)
        {
            CUCarta ficha = new CUCarta();
            ficha.Location = new Point((casillero.X * ficha.Width) + casillero.Posx, (casillero.Y * ficha.Height) + casillero.Posy);
            ficha.Casillero = casillero;
            ficha.EstablecerImagen(casillero.Imagen);
            ficha.FichaClick += Ficha_FichaClick;
            this.Controls.Add(ficha);

        }


        private void Ficha_FichaClick(CUCarta ficha)
        {
            
            var cartasDeLaMano = 0;
            var cartaSeleccionada = new BE.Carta { Palo = ficha.Casillero.Palo, Numero = ficha.Casillero.Numero };
            if (t.Mazojugador1().Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero))
            {
                cartasDeLaMano++;
            }
            if (t.Mazojugador2().Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero))
            {
                cartasDeLaMano++;
            }
            //var aaa = ficha.Casillero;
            //juego.SeleccionarCasillero(ficha.Casillero);
            if (!ListaSeleccionada.Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero) && cartasDeLaMano <= 1)
            {
                ListaSeleccionada.Add(cartaSeleccionada);
            }
        }

        private void btnRepartir_Click(object sender, EventArgs e)
        {
            Prueba();
          
            tablero.EnviarCasillero += Tablero_EnviarCasillero;
            t.Repartir(ListMazo);
            //dataGridView1.DataSource = null;
            //dataGridView2.DataSource = null;
            //dataGridView1.DataSource = t.Mazojugador1();
            //dataGridView2.DataSource = t.Mazojugador2();

            tablero.Repartir(t.Mazojugador1(), 0, 100, 0);
            tablero.Repartir(t.Mazojugador2(), 1, 100, 300);
            
            // t.Repartir(ListMazo);
            //dataGridView1.DataSource= t.Mazojugador1();
            // dataGridView2.DataSource= t.Mazojugador2();
        }
    }
}
