﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Escoba_de_15
{
    public partial class PantallaPrincipal : Form
    {
        BLL.Mazo m = new BLL.Mazo();
        BE.Mazo mazo;
        BLL.Turno t = new BLL.Turno();
        List<BE.Carta> ListMazo = new List<BE.Carta>();
        List<BE.Carta> ListMazo2 = new List<BE.Carta>();

        List<BE.Carta> ListaSeleccionada = new List<BE.Carta>();
        BLL.Tablero tablero = new BLL.Tablero();
        public PantallaPrincipal()
        {
            InitializeComponent();
            m.CargarPalos();
            m.CrearMazo();
            //listBox1.DataSource = m.DevuelveMazo();
            dataGridView1.DataSource = null;
            dataGridView2.DataSource = null;
            ListMazo = m.DevuelveMazo();
            ListMazo2.Add(ListMazo.First());
            dataGridView1.DataSource = ListMazo;
            //dataGridView2.DataSource = m.Mezclar(m.DevuelveMazo());

        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {
          
        }

        private void ingresarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ingresar ingresar = new Ingresar();
            ingresar.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView2.DataSource = null;
           // m.Repartir();
            dataGridView2.DataSource = m.Mezclar(ListMazo);
        }

        private void jugarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormTablero tab = new FormTablero();
            tab.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridView3.DataSource = null;
            dataGridView4.DataSource = null;
            dataGridView5.DataSource = null;
            //t.Repartir(ListMazo);
            tablero.EnviarCasillero += Tablero_EnviarCasillero;
            //var cartasJ1 = t.Mazojugador1();
            //var cartasJ1 = new List <BE.Carta>();
            //dataGridView4.DataSource = cartasJ1;
            t.Repartir(ListMazo);
            dataGridView4.DataSource = t.Mazojugador1();
            dataGridView5.DataSource = t.Mazojugador2();
            var aaa = t.CartasMesa(ListMazo);
            dataGridView3.DataSource = aaa;
            //dataGridView3.DataSource = t.CartasMesa(ListMazo);

            tablero.Repartir(t.Mazojugador1(), 0,500,0);
            tablero.Repartir(t.Mazojugador2(), 1,500,200);
            tablero.Repartir(aaa, 1,200,100);
            
            //cuCarta1.EstablecerImagen("hola");

        }

        private void button3_Click(object sender, EventArgs e)
        {
            t.Repartir(ListMazo);
            dataGridView4.DataSource = t.Mazojugador1();

            dataGridView5.DataSource = t.Mazojugador2();
          
           
            // tablero.Repartir(ListMazo2,1);
        }
        private void Tablero_EnviarCasillero(Casillero casillero)
        {
            CUCarta ficha = new CUCarta();
            ficha.Location = new Point((casillero.X * ficha.Width) + casillero.Posx , (casillero.Y * ficha.Height) + casillero.Posy );
            ficha.Casillero = casillero;
            ficha.EstablecerImagen(casillero.Imagen);
            ficha.FichaClick += Ficha_FichaClick;

            this.Controls.Add(ficha);

        }


        private void Ficha_FichaClick(CUCarta ficha)
        {
            var cartasDeLaMano = 0;
            var cartaSeleccionada = new BE.Carta { Palo = ficha.Casillero.Palo, Numero = ficha.Casillero.Numero };
            if (t.Mazojugador1().Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero))
            {
                cartasDeLaMano++;
            }
            if (t.Mazojugador2().Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero))
            {
                cartasDeLaMano++;
            }
            //var aaa = ficha.Casillero;
            //juego.SeleccionarCasillero(ficha.Casillero);
            if (!ListaSeleccionada.Any(x => x.Palo == cartaSeleccionada.Palo && x.Numero == cartaSeleccionada.Numero) && cartasDeLaMano <= 1)
            {
                ListaSeleccionada.Add(cartaSeleccionada);
            }
        }


        private void PantallaPrincipal_Shown(object sender, EventArgs e)
        {
            //string ruta = ".\\imagenes\\espada.jpg ";
            //foreach (Control c in this.Controls)
            //{

            //    ((CUCarta)c).EstablecerImagen(ruta);


            //}
        }

        private void cuCarta1_Click(object sender, EventArgs e)
        {
            
                }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }
    }
}
